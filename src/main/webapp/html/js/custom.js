if (typeof Object.create !== 'function') {
    Object.create = function( object ) {
        function F(){};
        F.prototype = object;
        return new F();
    };
}

(function($, window, document, undefined) {

    var TagComponent = {

        init: function(options) {

            this.options =  $.extend({}, $.fn.activateTag.options, options);

            this.buttonId =     this.options.buttonId;
            this.selectId =     this.options.selectId;
            this.containerId =  this.options.containerId;
            this.hiddenId =     this.options.hiddenId;

            this._bindListeners();
        },

        _bindListeners: function() {
            var scope = this;
            scope._addTagsListener();
            scope._removeTagsListener();
        },

        _addTagsListener: function(){
            var scope = this;
            $(document).on("click", this.buttonId, function(){
                var selectedOption = $(scope.selectId).find(":selected");
                var name = selectedOption.text();
                var id = selectedOption.val();
                var tagHtml = "<div data-id='" + id + "' data-name='" + name + "' class='alert alert-info inline'> " +
                    name + " <span class='deleteTag close'>&times;</span>" +
                    "<input type=\"hidden\" name=\"" + scope.hiddenId + "[0][id]\" value=\"" + id  + "\" ></div> ";
                $(scope.containerId).append(tagHtml);
                selectedOption.remove();
                if( $(scope.selectId).find("option").length == 0){
                    $(scope.selectId).parent().css("display","none");
                }
                scope._reindexHiddenFields(scope.hiddenId, scope.containerId);
            });
        },

        _removeTagsListener: function(){
            var scope = this;
            $(document).on("click", ".deleteTag", function(){
                var element = $(this);
                var parent = element.parent();
                var id = parent.attr("data-id");
                var name = parent.attr("data-name");
                var selectElement = $(scope.selectId);
                if(selectElement.parent().css("display") == "none"){
                    selectElement.parent().css("display","block");
                }
                $("<option></option>", {
                    value: id,
                    text: name
                }).appendTo(selectElement);
                parent.remove();
                scope._reindexHiddenFields(scope.hiddenId, scope.containerId);
            });
        },

        _reindexHiddenFields: function(hiddenId, containerId) {
            var hiddenFields = $(containerId).find("input[type='hidden']");
            $.each(hiddenFields, function(index, element){
                var name = hiddenId + "[" + index + "][id]";
                $(element).attr("name", name);
            });
        }
    };

    /*=== plugin method ===*/
    $.fn.activateTag = function(options) {

        return this.each(function(){

            var tagComponent = Object.create(TagComponent);

            tagComponent.init(options);
        });
    };

    /*=== default options ===*/
    $.fn.activateTag.options = {
        buttonId: "#addOrganizationButton",
        selectId: "#availableOrganizations",
        containerId: "#addedOrganizations",
        hiddenId: "organizations"
    };

})(jQuery, window, document);


jQuery(document).ready(function() {

    $(document).activateTag({
        buttonId: "#addOrganizationButton",
        selectId: "#availableOrganizations",
        containerId: "#addedOrganizations",
        hiddenId: "organizations"
    });

    $(document).activateTag({
        buttonId: "#addUserGroupsButton",
        selectId: "#availableUserGroups",
        containerId: "#addedUserGroups",
        hiddenId: "userGroups"
    });

    $("body").on("click", "a[id*='invite']", function(){
        var inviteId = $(this).data("invite"),
            countNetwork = $("input[id*='networks']").length;
        $(this).prev().remove();
        $(this).replaceWith("<span class='btn disabled'>"+$(this).text()+"</span>");
        var hiddenInput = "<input type='hidden' value='"+inviteId+"' name='networks["+ countNetwork +"][id]' id='networks["+ countNetwork +"][id]' />"
        $("#invite-list").append(hiddenInput);
    });
});