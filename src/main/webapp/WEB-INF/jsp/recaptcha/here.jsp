<%-- Bad, bad... Injecting scripts all over the HTTPS --%>
<script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=${captchaPublicKey}"></script>
<noscript>
	<iframe src="http://www.google.com/recaptcha/api/noscript?k=${captchaPublicKey}" height="300" width="500" frameborder="0"></iframe><br>
	<textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
	<input type="hidden" name="recaptcha_response_field" value="manual_challenge">
</noscript>