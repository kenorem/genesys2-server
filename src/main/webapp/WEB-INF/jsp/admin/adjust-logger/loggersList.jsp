<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title><spring:message code="adjust.loggers.list.page"/></title>
</head>
<body>
<h1><spring:message code="adjust.loggers.list.page"/></h1>

<div class="main-col-header clearfix">
    <div class="nav-header pull-left">
        <div class="results"><spring:message code="paged.totalElements" arguments="${totalPage}" /></div>
        <div class="pagination">
            <spring:message code="paged.pageOfPages" arguments="${pageNumber},${totalPage}" />
            <a class="${pageNumber-1 eq 0 ? 'disabled' :''}" href="?page=${pageNumber-1 eq 0 ? 1 : pageNumber-1}"><spring:message code="pagination.previous-page" /></a> <a href="?page=${pageNumber + 1}"><spring:message code="pagination.next-page" /></a>
        </div>
    </div>
</div>

<div>

    <div class="change-level-label">Add logger</div>

    <form method="post" class="form-change-level form-inline" action=<c:url value="/admin/addLoger"/> >

        <div class="form-group col-sm-3">
            <input class="form-control" type="text" name="nameNewLogger" placeholder="com.example.package.Class"/>
        </div>

        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

        <div class="form-group col-sm-3">
            <input type="submit" class="btn btn-group-sm">
        </div>

    </form>
</div>

<table class="all-loggers">
    <thead>
    <tr>
        <td><spring:message code="logger.name" /></td>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${loggers}" var="logger" varStatus="status">
        <tr class="${status.count % 2 == 0 ? 'even' : 'odd'} particular-logger">
            <td><a class="adjust-logger-config" href="<c:url value="/admin/adjustLoggerPage/${logger.name}." />">${logger.name}</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
