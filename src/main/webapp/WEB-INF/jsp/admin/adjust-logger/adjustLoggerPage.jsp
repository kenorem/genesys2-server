<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title><spring:message code="adjust.logger.edit.page"/></title>
</head>

<body>
<h1><spring:message code="adjust.logger.edit.page"/></h1>

<div class="logger-detail">
  <div class="logger-name">
    <div>Loger name - ${logger.name}</div>
  </div>
  <div class="logger-level">
    <div>Loger level - ${logger.level}</div>
  </div>
  <div class="all-appenders">
    Appenders:
    <ul>
    <c:forEach items="${appenders}" var="appender">
        <li>
            ${appender.name}
        </li>
    </c:forEach>
    </ul>
  </div>
  <div>

    <div class="change-level-label">Change level</div>

    <form method="post" class="form-change-level form-inline" action=<c:url value="/admin/changeLoger"/> >

      <input type="hidden" name="loggerName" value="${logger.name}"/>

      <div class="form-group">
        <select class="form-control" name="loggerLevel">
          <c:forTokens items="all,debug,info,warn,error,fatal,off,trace" delims="," var="level">
            <option  value="${level}">
                ${level}
            </option>
          </c:forTokens>
        </select>
      </div>

      <!-- CSRF protection -->
      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

      <div class="form-group">
        <input type="submit" class="btn btn-group-sm">
      </div>

      <div class="form-group">
        <a class="btn btn-default" href="<c:url value="/admin/adjustLogger" />">Cancel</a>
      </div>

    </form>
  </div>
</div>
</body>
</html>
