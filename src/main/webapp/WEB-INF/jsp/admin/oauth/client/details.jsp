<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="oauth-client.page.profile.title" arguments="${item.clientId}" argumentSeparator="|" /></title>
</head>
<body>
	<h1>
		<c:out value="${item.clientId}" />
	</h1>

	<div>
		<c:forEach items="${item.authorities}" var="authority">
			<c:out value="${authority}" />
		</c:forEach>
	</div>
	<div>
		<c:forEach items="${item.authorizedGrantTypes}" var="authGrantType">
			<c:out value="${authGrantType}" />
		</c:forEach>
	</div>
	<div>
		<c:out value="${item.clientSecret}" />
	</div>

	<h2><spring:message code="oauth-client.active-tokens" /></h2>
	<ul class="funny-list">
		<c:forEach items="${tokens}" var="token">
			<li><c:out value="${token}" /></li>
		</c:forEach>
	</ul>
</body>
</html>