<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="descriptor.category" /></title>
</head>
<body>
	<h1>
		<spring:message code="descriptor.category" />
	</h1>

	<table>
			<thead>
			<tr>
				<td><spring:message code="descriptor.category" /></td>
				<td><spring:message code="ce.sameAs" /></td>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${categories}" var="category" varStatus="status">
			<tr class="${status.count % 2 == 0 ? 'even' : 'odd'}">
				<td><a href="<c:url value="/category/${category.id}" />"><c:out value="${category.getName(pageContext.response.locale)}" /></a></td>
				<td>
				<c:if test="${category.rdfUri ne null}">
				   <a href="<c:url value="${category.rdfUri}" />">
				      <c:out value="${category.getRdfUriId()}" />
				   </a>
				</c:if>
				</td>
			</tr>
 		</c:forEach>
		</tbody>
	</table>

</body>
</html>