<?xml version="1.0" encoding="UTF-8"?>
<!--
  Copyright 2014 Global Crop Diversity Trust
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
    http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<web-app 
    xmlns="http://java.sun.com/xml/ns/javaee" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
    version="3.0">

    <!--<context-param>-->
        <!--<param-name>contextConfigLocation</param-name>-->
        <!--<param-value>classpath:spring/application-context.xml</param-value>-->
    <!--</context-param>-->

    <context-param>
        <param-name>contextClass</param-name>
        <param-value>
            org.springframework.web.context.support.AnnotationConfigWebApplicationContext
        </param-value>
    </context-param>
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>org.genesys2.spring.config.ApplicationConfig</param-value>
    </context-param>

	<listener>
	    <listener-class>com.hazelcast.web.SessionListener</listener-class>
	</listener>

    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>
   
	<filter>
		<filter-name>UrlRewriteFilter</filter-name>
		<filter-class>org.tuckey.web.filters.urlrewrite.UrlRewriteFilter</filter-class>
		<init-param>
			<param-name>confReloadCheckInterval</param-name>
			<param-value>10</param-value>
		</init-param>
		<!-- <init-param>
			<param-name>logLevel</param-name>
			<param-value>sysout:DEBUG</param-value>
		</init-param> -->
	</filter>
	<filter-mapping>
	    <filter-name>UrlRewriteFilter</filter-name>
	    <url-pattern>/*</url-pattern>
	    <dispatcher>REQUEST</dispatcher>
	    <dispatcher>FORWARD</dispatcher>
	</filter-mapping>

	<filter>
		<filter-name>localeURLFilter</filter-name>
		<filter-class>org.genesys2.server.servlet.filter.LocaleURLFilter</filter-class>
		<init-param>
			<param-name>exclude-paths</param-name>
			<param-value>/html</param-value>
		</init-param>
		<init-param>
			<param-name>default-locale</param-name>
			<param-value>en</param-value>
		</init-param>
		<init-param>
			<param-name>allowed-locales</param-name>
			<param-value>en es de fr fa ar ru zh pt</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>localeURLFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>
	
	<filter>
		<filter-name>encodingFilter</filter-name>
		<filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
		<init-param>
			<param-name>encoding</param-name>
			<param-value>UTF-8</param-value>
		</init-param>
		<init-param>
			<param-name>forceEncoding</param-name>
			<param-value>true</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>encodingFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>
	
	<!-- Cross origin for web REST API clients -->
	<filter>
        <filter-name>cross-origin</filter-name>
        <filter-class>org.eclipse.jetty.servlets.CrossOriginFilter</filter-class>
        <!-- <init-param>
	        <param-name>allowedOrigins</param-name>
	        <param-value>*</param-value>
	    </init-param> -->
<!--         <init-param>
        	<param-name>allowCredentials</param-name>
        	<param-value>true</param-value>
        </init-param>
 -->
        <init-param>
        	<param-name>allowedMethods</param-name>
        	<param-value>GET,POST,PUT,DELETE</param-value>
        </init-param>
 		<init-param>
	        <param-name>allowedHeaders</param-name>
	        <param-value>authorization,content-type</param-value>
	    </init-param>
	    <!-- Do not chain preflight request to application -->
        <init-param>
	        <param-name>chainPreflight</param-name>
	        <param-value>false</param-value>
	    </init-param>
    </filter>
	<filter-mapping>
	    <filter-name>cross-origin</filter-name>
	    <url-pattern>/oauth/token</url-pattern>
	</filter-mapping>
	<filter-mapping>
	    <filter-name>cross-origin</filter-name>
	    <url-pattern>/api/*</url-pattern>
	</filter-mapping>
	<filter-mapping>
	    <filter-name>cross-origin</filter-name>
	    <url-pattern>/webapi/*</url-pattern>
	</filter-mapping>
	
	
	
	<filter>
        <filter-name>hazelcastWebFilter</filter-name>
        <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
        <init-param>
            <param-name>targetFilterLifecycle</param-name>
            <param-value>true</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>hazelcastWebFilter</filter-name>
        <url-pattern>/*</url-pattern>
        <dispatcher>FORWARD</dispatcher>
	    <dispatcher>INCLUDE</dispatcher>
    	<dispatcher>REQUEST</dispatcher>
    </filter-mapping>

    <filter>
        <filter-name>org.springframework.security.filterChainProxy</filter-name>
        <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>org.springframework.security.filterChainProxy</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    
	<filter>
		<description>Adds variables to all requests</description>
        <filter-name>envVarFilter</filter-name>
        <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>envVarFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

    <filter>
        <description>Genesys Web API Filter</description>
        <filter-name>webApiFilter</filter-name>
        <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>webApiFilter</filter-name>
        <url-pattern>/webapi/*</url-pattern>
    </filter-mapping>
	
	<filter>
		<filter-name>sitemesh</filter-name>
		<filter-class>org.sitemesh.config.ConfigurableSiteMeshFilter</filter-class>
	</filter>
    <filter-mapping>
		<filter-name>sitemesh</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>

    <servlet>
        <servlet-name>mvc</servlet-name>
        <servlet-class>
            org.springframework.web.servlet.DispatcherServlet
        </servlet-class>
        <init-param>
            <param-name>contextClass</param-name>
            <param-value>
                org.springframework.web.context.support.AnnotationConfigWebApplicationContext
            </param-value>
        </init-param>
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>
                org.genesys2.spring.config.SpringServletConfig
            </param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>
    
    <servlet>
		<servlet-name>default</servlet-name>
		<init-param>
			<param-name>dirAllowed</param-name>
			<param-value>false</param-value>
		</init-param>
	</servlet>

	<!-- Use default Jetty servlet for static resources in /html/* -->
	<servlet-mapping>
		<servlet-name>default</servlet-name>
		<url-pattern>/html/*</url-pattern>
	</servlet-mapping>

    <servlet-mapping>
        <servlet-name>mvc</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>

    <welcome-file-list>
        <welcome-file>/index.html</welcome-file>
    </welcome-file-list>

	<error-page>
		<error-code>401</error-code>  
		<location>/WEB-INF/jsp/errors/401.jsp</location>  
	</error-page>
    <error-page>
		<error-code>404</error-code>  
		<location>/WEB-INF/jsp/errors/404.jsp</location>  
	</error-page>
    <error-page>
		<error-code>500</error-code>  
		<location>/WEB-INF/jsp/errors/500.jsp</location>  
	</error-page>
    <error-page>
		<error-code>503</error-code>  
		<location>/WEB-INF/jsp/errors/503.jsp</location>  
	</error-page>
    <session-config>
        <session-timeout>30</session-timeout>
        <tracking-mode>COOKIE</tracking-mode>
    </session-config>

	<jsp-config>
	    <jsp-property-group>
	        <url-pattern>*.jsp</url-pattern>
	        <page-encoding>UTF-8</page-encoding>
	    </jsp-property-group>
	</jsp-config>
</web-app>
