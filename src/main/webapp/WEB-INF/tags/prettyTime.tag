<%@ tag description="Pretty print a date" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ tag import="org.ocpsoft.prettytime.PrettyTime"  %>
<%@ attribute name="date" required="true" type="java.util.Date" %>
<%@ attribute name="locale" required="true" type="java.util.Locale" %>
<%
	PrettyTime p = new PrettyTime(locale);
	%><%= p.format(date) %><%

%> <small><fmt:formatDate value="${date}" /></small>