<%@include file="/WEB-INF/jsp/init.jsp"%>

<c:choose>
	<c:when test="${requestContext.theme.name eq 'one'}">
<link href="<c:url value="/html/css/all.min.css" />" rel="stylesheet" />
	</c:when>
	<c:when test="${requestContext.theme.name eq 'all-min'}">
<link href="<c:url value="/html/css/bootstrap.min.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/jquery-ui.min.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/leaflet.min.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/leaflet.locationfilter.min.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/genesys.min.css" />" rel="stylesheet" />
	</c:when>
	<c:when test="${requestContext.theme.name eq 'all'}">
<link href="<c:url value="/html/css/bootstrap.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/jquery-ui.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/leaflet.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/leaflet.locationfilter.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/syronex-colorpicker.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/genesys.css" />" rel="stylesheet" />
	</c:when>
	<c:otherwise>
<link href="<c:url value="/html/css/bootstrap.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/jquery-ui.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/leaflet.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/leaflet.locationfilter.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/custom.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/responsive.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/forza.css" />" rel="stylesheet" />
<link href="<c:url value="/html/css/syronex-colorpicker.css" />" rel="stylesheet" />
 	</c:otherwise>
</c:choose>
