#-------------------------------------------------------------------------------
# Copyright 2014 Global Crop Diversity Trust
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

# Errors
http-error.401=No autorizado
http-error.401.text=Se requiere autenticación.
http-error.403=Acceso denegado
http-error.403.text=No tiene permiso para acceder al recurso.
http-error.404=No encontrado
http-error.404.text=El recurso solicitado no se ha encontrado, pero podría volver a estar disponible en el futuro.
http-error.500=Error interno del servidor
http-error.500.text=Se ha encontrado una situación inesperada. No hay otro mensaje específico más adecuado.
http-error.503=Servicio no disponible
http-error.503.text=El servidor no está disponible en estos momentos (por sobrecarga o mantenimiento).


# Login
login.username=Nombre de usuario
login.password=Contraseña
login.invalid-credentials=Acreditación inválida.
login.remember-me=Recordar mis datos de usuario
login.login-button=Iniciar sesión
login.register-now=Crear una cuenta
logout=Cerrar sesión
login.forgot-password=He olvidado mi contraseña
login.with-google-plus=Iniciar sesión con Google+

# Registration
registration.page.title=Crear una cuenta de usuario
registration.title=Crear su cuenta
registration.invalid-credentials=Acreditación inválida.
registration.user-exists=Nombre de usuario ya utilizado.
registration.email=Correo electrónico
registration.password=Contraseña
registration.confirm-password=Repetir contraseña
registration.full-name=Nombre completo
registration.create-account=Crear cuenta
captcha.text=Texto de la imagen


id=Identificación

name=Nombre
description=Descripción
actions=Acciones
add=Agregar
edit=Editar
save=Guardar
create=Crear
cancel=Cancelar
delete=Borrar

jump-to-top=Volver arriba

pagination.next-page=Siguiente >
pagination.previous-page=< Anterior


# Language
locale.language.change=Cambio de ubicación
i18n.content-not-translated=El contenido de esta página no está disponible en su idioma. Póngase en contacto con nosotros si puede ayudarnos a traducirlo.

data.error.404=La información solicitada no se ha encontrado en el sistema.
page.rendertime=Esta página se procesó en {0} ms.

footer.copyright-statement=&copy; proveedores de datos y GCDT 2013

menu.home=Página de inicio
menu.browse=Navegar
menu.datasets=Datos de caracterización y evaluación
menu.descriptors=Descriptores
menu.countries=Países
menu.institutes=Instituciones
menu.my-list=Mi lista
menu.about=Acerca de Genesys
menu.contact=Póngase en contacto con nosotros
menu.disclaimer=Cláusula de protección
menu.feedback=Comentarios
menu.help=Ayuda
menu.terms=Términos y condiciones de uso
menu.copying=Política de derechos de autor
menu.privacy=Política de privacidad

page.home.title=Recursos fitogenéticos de Genesys

user.pulldown.administration=Administración
user.pulldown.users=Lista de usuarios
user.pulldown.logout=Cerrar sesión
user.pulldown.profile=Ver perfil
user.pulldown.oauth-clients=Clientes OAuth
user.pulldown.teams=Equipos

user.pulldown.heading={0}
user.create-new-account=Crear una cuenta de usuario
user.full-name=Nombre completo
user.email=Dirección de correo electrónico
user.account-status=Estado de cuenta
user.account-disabled=Cuenta deshabilitada
user.account-locked-until=Cuenta bloqueada hasta
user.roles=Papeles del usuario
userprofile.page.title=Perfil de usuario
userprofile.update.title=Actualice su perfil

user.page.list.title=Cuentas de usuarios registradas

crop.croplist=Lista de cultivos
crop.all-crops=Todos los cultivos
crop.page.profile.title={0} perfil
crop.taxonomy-rules=Reglas taxonómicas
crop.view-descriptors=Ver descriptores de cultivos...

activity.recent-activity=Actividad reciente

country.page.profile.title=Perfil del país\: {0}
country.page.list.title=Lista de países
country.page.not-current=Esta es una entrada histórica
country.page.faoInstitutes={0} instituciones registradas en WIEWS
country.stat.countByLocation={0} accesiones conservadas en instituciones de este país
country.stat.countByOrigin={0} accesiones registradas en Genesys provienen de este país.
country.statistics=Estadísticas del país
country.accessions.from=Accesiones recogidas en {0}
country.more-information=Más información\:
country.replaced-by=El código de país se ha reemplazado por\: {0}
country.is-itpgrfa-contractingParty={0} es parte del Tratado internacional sobre los recursos fitogenéticos para la alimentación y la agricultura (TIRFAA).
select-country=Seleccionar país

faoInstitutes.page.list.title=Instituciones miembros de WIEWS
faoInstitutes.page.profile.title=WIEWS {0} - Sistema Mundial de Información y Alerta sobre los Recursos Fitogenéticos para la Alimentación y la Agricultura
faoInstitutes.stat.accessionCount=Accesiones en Genesys\: {0}.
faoInstitutes.stat.datasetCount=Conjuntos de información adicionales en Genesys\: {0}.
faoInstitute.stat-by-crop=Cultivos más representados
faoInstitute.stat-by-genus=Géneros más representados
faoInstitute.stat-by-species=Especies más representadas
faoInstitute.accessionCount={0} accesiones
faoInstitute.statistics=Estadísticas institucionales
faoInstitutes.page.data.title=Accesiones en {0}
faoInstitute.accessions.at=Accesiones en {0}
faoInstitutes.viewAll=Ver todas las instituciones registradas
faoInstitutes.viewActiveOnly=Ver instituciones con accesiones en Genesys
faoInstitute.no-accessions-registered=Póngase en contacto con nosotros si puede proveer información desde esta institución.
faoInstitute.institute-not-current=Este registro se ha archivado.
faoInstitute.view-current-institute=Ir a {0}.
faoInstitute.country=País
faoInstitute.code=Código en WIEWS
faoInstitute.email=Correo electrónico de contacto
faoInstitute.acronym=Siglas
faoInstitute.url=Enlace web
faoInstitute.member-of-organizations-and-networks=Organizaciones y grupos de trabajo\:
faoInstitute.uniqueAcceNumbs.true=Todos los números de identificación de una accesión son únicos dentro de esta institución.
faoInstitute.uniqueAcceNumbs.false=El mismo número de accesión puede haberse utilizado en distintas colecciones de esta institución.
faoInstitute.requests.mailto=Dirección de correo electrónico para las solicitudes de germoplasma\:
faoInstitute.allow.requests=Permitir peticiones de material
faoInstitute.sgsv=Código SGSV

view.accessions=Ver accesiones
view.datasets=Ver conjuntos de datos
paged.pageOfPages=Página {0} de {1}
paged.totalElements={0} entradas

accessions.number={0} accesiones
accession.metadatas=Datos de caracterización y evaluación
accession.methods=Datos de caracterización y evaluación
unit-of-measure=Unidad de medida

ce.trait=Rasgo característico
ce.sameAs=Al igual que
ce.methods=Métodos
ce.method=Método
method.fieldName=Campo de la base de datos

accession.uuid=Identificador Universalmente Único
accession.accessionName=Número de accesión
accession.origin=País de origen
accession.holdingInstitute=Institución poseedora
accession.holdingCountry=Ubicación
accession.taxonomy=Nombre científico
accession.crop=Nombre del cultivo
accession.otherNames=También conocido como
accession.inTrust=En "Trust"
accession.mlsStatus=Estado del Servicio de Listado Múltiple (MLS)
accession.duplSite=Institución de duplicado de seguridad
accession.inSvalbard=Duplicado de seguridad en Svalbard 
accession.inTrust.true=Esta accesión está sujeta al Artículo 15 del Tratado internacional sobre los recursos fitogenéticos para la alimentación y la agricultura.
accession.mlsStatus.true=Esta accesión se incluye en el Sistema multilateral del Tratado internacional sobre los recursos fitogenéticos para la alimentación y la agricultura.
accession.inSvalbard.true=Duplicado de seguridad en la Bóveda Global de Semillas de Svalbard.
accession.not-available-for-distribution=Accesión NO disponible para su distribución.
accession.available-for-distribution=La accesión está disponible para su distribución.
accession.elevation=Elevación
accession.geolocation=Ubicación geográfica (lat., long.)

accession.storage=Tipo de almacenamiento del germoplasma
accession.storage.=
accession.storage.10=Colección de semillas
accession.storage.11=Corto plazo
accession.storage.12=Medio plazo
accession.storage.13=Largo plazo
accession.storage.20=Colección de campo
accession.storage.30=Colección in vitro
accession.storage.40=Colección criopreservada
accession.storage.50=Colección de ADN
accession.storage.99=Otro

accession.breeding=Información del criador
accession.breederCode=Código del criador
accession.pedigree=Genealogía
accession.collecting=Información de la colección
accession.collecting.site=Localización del sitio de colección
accession.collecting.institute=Institución colectora
accession.collecting.number=Número de colección
accession.collecting.date=Fecha de colección de la muestra
accession.collecting.mission=Identificador de la campaña de recolección
accession.collecting.source=Colección/Origen de la adquisición

accession.collectingSource.=
accession.collectingSource.10=Hábitat silvestre
accession.collectingSource.11=Bosque o arboleda
accession.collectingSource.12=Matorral
accession.collectingSource.13=Pastizal
accession.collectingSource.14=Desierto o tundra
accession.collectingSource.15=Hábitat acuático
accession.collectingSource.20=Hábitat de cultivo o de campo
accession.collectingSource.21=Campo
accession.collectingSource.22=Huerto
accession.collectingSource.23=Huerto familiar (urbano, de la periferia o rural), jardín o patio trasero
accession.collectingSource.24=Terreno de barbecho
accession.collectingSource.25=Pastura
accession.collectingSource.26=Almacén de compras rural
accession.collectingSource.27=Residuos de cosecha
accession.collectingSource.28=Parque
accession.collectingSource.30=Mercado o tienda
accession.collectingSource.40=Instituto, Estación experimental, Organización de investigación, Banco de genes
accession.collectingSource.50=Empresa semillera
accession.collectingSource.60=Área ruderal o perturbada, hábitat de maleza
accession.collectingSource.61=Arcén
accession.collectingSource.62=Margen de campo
accession.collectingSource.99=Otro

accession.donor.institute=Institución donante
accession.donor.accessionNumber=Identificador del donante de la accesión
accession.geo=Información geográfica
accession.geo.datum=Datos de coordenadas
accession.geo.method=Método de georreferenciación
accession.geo.uncertainty=Incertidumbre de coordenadas
accession.sampleStatus=Estado biológico de la accesión

accession.sampleStatus.=
accession.sampleStatus.100=Silvestre
accession.sampleStatus.110=Natural
accession.sampleStatus.120=Seminatural/Silvestre
accession.sampleStatus.130=Seminatural/cultivado
accession.sampleStatus.200=Maleza
accession.sampleStatus.300=Variedad cultivada tradicional/local
accession.sampleStatus.400=Germoplasma para cultivo/investigación
accession.sampleStatus.410=Línea de criadores
accession.sampleStatus.411=Población sintética
accession.sampleStatus.412=Híbrido
accession.sampleStatus.413=Stock genético fundador/población base
accession.sampleStatus.414=Línea consanguínea
accession.sampleStatus.415=Población segregante
accession.sampleStatus.416=Selección clónica
accession.sampleStatus.420=Stock genético
accession.sampleStatus.421=Mutante
accession.sampleStatus.422=Reservas citogenéticas
accession.sampleStatus.423=Otros stocks genéticos
accession.sampleStatus.500=Variedad cultivada avanzada/mejorada
accession.sampleStatus.600=OMG (Organismo genéticamente modificado)
accession.sampleStatus.999=Otro

accession.availability=Disponibilidad para distribución
accession.aliasType.ACCENAME=Nombre de accesión
accession.aliasType.DONORNUMB=Identificador del donante de la accesión
accession.aliasType.BREDNUMB=Nombre asignado por el criador
accession.aliasType.COLLNUMB=Número de coleción
accession.aliasType.OTHERNUMB=Otros nombres
accession.aliasType.DATABASEID=(Identificador de base de datos)
accession.aliasType.LOCALNAME=(Nombre local)

accession.availability.=Desconocido
accession.availability.true=Disponible
accession.availability.false=No disponible

accession.page.profile.title=Perfil de la accesión\: {0}
accession.page.resolve.title=Se han encontrado múltiples accesiones
accession.resolve=Se han encontrado múltiples accesiones con el nombre ''{0}'' en Genesys. Seleccione una de la lista.
accession.page.data.title=Navegador de accesiones
accession.taxonomy-at-institute=Ver {0} en {1}

accession.svalbard-data=Información de duplicados en la Bóveda Global de Semillas de Svalbard
accession.svalbard-data.taxonomy=Nombre científico indicado
accession.svalbard-data.depositDate=Fecha de depósito
accession.svalbard-data.boxNumber=Número de caja
accession.svalbard-data.quantity=Cantidad
accession.remarks=Observaciones

taxonomy.genus=Género
taxonomy.species=Especie
taxonomy.taxonName=Nombre científico
taxonomy-list=Lista taxonómica

selection.page.title=Accesiones seleccionadas
selection.add=Agregar {0} a la lista
selection.remove=Eliminar {0} de la lista
selection.clear=Borrar la lista
selection.empty-list-warning=No ha agregado accesiones a la lista.
selection.add-many=Revisar y agregar
selection.add-many.accessionIds=Lista de identificadores de accesiones en Genesys separados por espacios o en una nueva línea. 

savedmaps=Recordar mapa actual
savedmaps.list=Lista de mapas
savedmaps.save=Recordar mapa

filter.enter.title=Introduzca el título de filtro
filters.page.title=Filtros de datos
filters.view=Filtros actuales
filter.filters-applied=Ha aplicado filtros.
filter.filters-not-applied=Puede filtrar los datos.
filters.data-is-filtered=Información filtrada.
filters.toggle-filters=Filtros
filter.taxonomy=Nombre científico
filter.art15=La accesión se recoge en el Artículo 15 del TIRFAA.
filter.acceNumb=Número de accesión
filter.alias=Nombre de accesión
filter.crops=Nombre del cultivo
filter.orgCty.iso3=País de origen
filter.institute.code=Nombre de la institución poseedora
filter.institute.country.iso3=País de la institución poseedora
filter.sampStat=Estatus biológico de la accesión
filter.institute.code=Nombre de la institución poseedora
filter.geo.latitude=Latitud
filter.geo.longitude=Longitud
filter.geo.elevation=Elevación
filter.taxonomy.genus=Género
filter.taxonomy.species=Especie
filter.taxSpecies=Especies
filter.taxonomy.sciName=Nombre científico
filter.sgsv=Duplicado de seguridad en Svalbard
filter.mlsStatus=Estado de Sistema de Listado Múltiple MLS de la accesión 
filter.available=Disponible para su distribución
filter.donorCode=Institución donante
filter.duplSite=Sitio de la duplicación de seguridad
filter.download-dwca=Descargar ZIP
filter.add=Añadir filtro
filter.additional=Filtros adicionales
filter.apply=Aplicar
filter.close=Cerrar
filter.remove=Eliminar filtro
filter.autocomplete-placeholder=Escriba más de 3 caracteres...
filter.coll.collMissId=ID de la misión de colecta
filter.storage=Tipo de almacenamiento del germoplasma
filter.string.equals=Igual
filter.string.like=Empieza con


search.page.title=Buscador de texto completo
search.no-results=No se han encontrado coincidencias para su búsqueda.
search.input.placeholder=Buscar en Genesys...
search.search-query-missing=Introduzca su consulta de búsqueda.
search.search-query-failed=Lo sentimos, se produjo un error en la búsqueda {0}
search.button.label=Buscar

admin.page.title=Administración de Genesys 2
metadata.page.title=Grupos de información
metadata.page.view.title=Detalles del grupo de información
metadata.download-dwca=Descargar ZIP
page.login=Iniciar sesión

traits.page.title=Descriptores
trait-list=Descriptores


organization.page.list.title=Organizaciones y grupos de trabajo
organization.page.profile.title={0}
organization.slug=Acrónimo de la organización o grupo de trabajo
organization.title=Nombre completo
filter.institute.networks=Red


menu.report-an-issue=Notificar un problema
menu.scm=Código de origen
menu.translate=Traducir Genesys

article.edit-article=Editando el artículo
article.slug=Página de artículo (URL)
article.title=Título del artículo
article.body=Cuerpo del artículo

activitypost=Publicación de actividad
activitypost.add-new-post=Agregar una nueva publicación
activitypost.post-title=Título de la publicación
activitypost.post-body=Cuerpo

blurp.admin-no-blurp-here=Sin descripción.
blurp.blurp-title=Título de la descripción
blurp.blurp-body=Contenidos de la descripción
blurp.update-blurp=Guardar descripción


oauth2.confirm-request=Confirmar acceso
oauth2.confirm-client=En virtud del presente escrito, usted, <b>{0}</b>, autoriza a acceder a <b>{1}</b> a sus recursos protegidos.
oauth2.button-approve=Sí, permitir acceso
oauth2.button-deny=No, denegar acceso

oauth2.authorization-code=Código de autorización
oauth2.authorization-code-instructions=Copiar el código de autorización\:

oauth2.access-denied=Acceso denegado
oauth2.access-denied-text=Ha denegado el acceso a sus recursos.

oauth-client.page.list.title=Clientes OAuth2 
oauth-client.page.profile.title=Cliente OAuth2\: {0}
oauth-client.active-tokens=Lista de muestras emitidas
client.details.title=Título del cliente
client.details.description=Descripción

maps.loading-map=Cargando mapa...
maps.view-map=Ver mapa
maps.accession-map=Mapa de las accesiones

audit.createdBy=Creado por {0}
audit.lastModifiedBy=Última actualización por {0}

itpgrfa.page.list.title=Partes del TIRFAA

request.page.title=Solicitar material de las instituciones poseedoras
request.total-vs-available=De {0} accesiones listadas, {1} están disponibles para su distribución.
request.start-request=Solicitar germoplasma disponible
request.your-email=Su dirección de correo electrónico\:
request.accept-smta=Aceptación del protocolo SMTA/MTA\:
request.smta-will-accept=Acepto los términos y condiciones del protocolo SMTA/MTA
request.smta-will-not-accept=NO acepto los términos y condiciones del protocolo SMTA/MTA
request.smta-not-accepted=No ha aceptado los términos y condiciones del protocolo SMTA/MTA. Su solicitud no será procesada.
request.purpose=Especifique el uso del material\:
request.purpose.0=Otros (describa en el campo para notas)
request.purpose.1=Investigación para la alimentación y la agricultura
request.notes=Añada cualquier comentario o nota adicional\:
request.validate-request=Valide su solicitud
request.confirm-request=Confirmar la recepción de la solicitud
request.validation-key=Clave de validación\:
request.button-validate=Validar
validate.no-such-key=La clave de validación no es válida.

team.user-teams=Equipos de los usuarios
team.create-new-team=Crear un nuevo grupo
team.team-name=Nombre del grupo
team.leave-team=Abandonar el grupo
team.team-members=Miembros del grupo
team.page.profile.title=Grupo\: {0}
team.page.list.title=Todos los grupos
validate.email.key=Introduzca la clave
validate.email=Validación del correo electrónico
validate.email.invalid.key=Contraseña no válida

edit-acl=Permisos de edición
acl.page.permission-manager=Administrador de permisos
acl.sid=Identidad de seguridad
acl.owner=Propietario del objeto
acl.permission.1=Leer
acl.permission.2=Escribir
acl.permission.4=Crear
acl.permission.8=Eliminar
acl.permission.16=Administrar


ga.tracker-code=Código de rastreo GA

boolean.true=Sí
boolean.false=No
boolean.null=Desconocido
userprofile.password=Restablecer contraseña
userprofile.enter.email=Introduzca su correo electrónico
userprofile.enter.password=Introduzca su nueva contraseña
userprofile.email.send=Enviar correo electrónico

verification.invalid-key=La clave utilizada no es válida.
verification.token-key=Clave de validación
login.invalid-token=El token de acceso no es válido

descriptor.category=Categoría del descriptor
method.coding-table=Tabla de codificación

oauth-client.info=Información del cliente
oauth-client.list=Lista de clientes de OAuth
client.details.client.id=Detalles del número de identificación del cliente
client.details.additional.info=Información adicional
client.details.token.list=Lista de muestras
client.details.refresh-token.list=Lista de tokens actualizadas
oauth-client.remove=Eliminar
oauth-client.remove.all=Eliminar todo
oauth-client=Cliente
oauth-client.token.issue.date=Fecha de emisión
oauth-client.expires.date=Fecha de expiración
oauth-client.issued.tokens=Tokens emitidos
client.details.add=Añadir cliente OAuth
oauth-client.create=Crear cliente OAuth
oauth-client.id=Número de identificación del cliente
oauth-client.secret=Secreto de cliente
oauth-client.redirect.uri=Redirección URI del cliente
oauth-client.access-token.accessTokenValiditySeconds=Validez del token de acceso
oauth-client.access-token.refreshTokenValiditySeconds=Actualizar validez del token
oauth-client.access-token.defaultDuration=Usar por defecto
oauth-client.title=Título del cliente
oauth-client.description=Descripción
oauth2.error.invalid_client=ID de cliente no válido. Validar los parámetros client_id y client_secret.

team.user.enter.email=Introduzca correo electrónico del usuario
user.not.found=Usuario no encontrado
team.profile.update.title=Actualizar la información del grupo

autocomplete.genus=Encontrar géneros...

stats.number-of-countries={0} Países
stats.number-of-institutes={0} Instituciones
stats.number-of-accessions={0} Accesiones

navigate.back=\\u21E0 Atrás


content.page.list.title=Lista de artículos
article.lang=Idioma
article.classPk=Objeto

session.expiry-warning-title=Advertencia de caducidad de la sesión
session.expiry-warning=Su sesión actual está a punto de caducar. ¿Desea continuar con esta sesión?
session.expiry-extend=Extender la sesión

download.kml=Descargar KML

data-overview=Resumen estadístico
data-overview.short=Resumen
data-overview.institutes=Instituciones poseedoras
data-overview.composition=Composición de la tenencia del banco de genes
data-overview.sources=Fuentes del material
data-overview.management=Gestión de la colección
data-overview.availability=Disponibilidad del material
data-overview.otherCount=Otros
data-overview.missingCount=No especificado
data-overview.totalCount=Total
data-overview.donorCode=Código WIEWS de la FAO del instituto donante
data-overview.mlsStatus=Disponible para su distribución según MLS
