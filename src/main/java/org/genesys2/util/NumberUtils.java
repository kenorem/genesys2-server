/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class NumberUtils {

	public static final Log LOG = LogFactory.getLog(NumberUtils.class);

	/**
	 * Utility to parse doubles
	 *
	 * @param doubleString
	 * @param factor
	 * @return
	 */
	public static Double parseDouble(final String doubleString, final double factor) {
		if (StringUtils.isBlank(doubleString)) {
			return null;
		}

		try {
			return Double.parseDouble(doubleString) / factor;
		} catch (final NumberFormatException e) {
			LOG.warn("Parse error: " + doubleString);
			return null;
		}
	}

	/**
	 * Utility to parse doubles, but ignores "0" values
	 *
	 * @param doubleString
	 * @param factor
	 * @return
	 */
	public static Double parseDoubleIgnore0(final String doubleString, final double factor) {
		if (StringUtils.isBlank(doubleString) || doubleString.trim().equals("0")) {
			return null;
		}

		try {
			return Double.parseDouble(doubleString) / factor;
		} catch (final NumberFormatException e) {
			LOG.warn("Parse error: " + doubleString);
			return null;
		}
	}

	/**
	 * Compare two Integers, Longs, or whatever
	 */
	public static <T> boolean areEqual(T a, T b) {
		return a == null && b == null || a != null && a.equals(b) || b != null && b.equals(a);
	}
}
