/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import java.util.Collection;

import org.genesys2.server.model.genesys.Metadata;
import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.genesys.Parameter;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.CropRule;
import org.genesys2.server.model.impl.CropTaxonomy;
import org.genesys2.server.model.impl.Team;
import org.genesys2.server.model.impl.User;
import org.springframework.data.domain.Page;

public class OAuth2Cleanup {

	public static <T> T clean(T t) {
		if (t instanceof User) {
			final User user = (User) t;
			user.setId(null);
			user.setPassword(null);
		}
		if (t instanceof Method) {
			final Method method = (Method) t;
			method.setParameter(null);
		}
		if (t instanceof Parameter) {
			final Parameter param = (Parameter) t;
			param.setCrop(null);
		}
		if (t instanceof Crop) {
			final Crop crop = (Crop) t;
			crop.setCropRules(null);
		}
		if (t instanceof CropRule) {
			final CropRule cropRule = (CropRule) t;
			cropRule.setCrop(null);
			cropRule.setId(null);
		}
		if (t instanceof CropTaxonomy) {
			final CropTaxonomy cropTaxa = (CropTaxonomy) t;
			cropTaxa.setCrop(null);
			cropTaxa.getTaxonomy().setCropTaxonomies(null);
		}
		if (t instanceof Team) {
			final Team team = (Team) t;
			team.setMembers(null);
			team.setInstitutes(null);
		}
		if (t instanceof Metadata) {
			// Metadata metadata = (Metadata) t;
		}
		return t;
	}

	public static <T> Collection<T> clean(Collection<T> x) {
		for (final T m : x) {
			clean(m);
		}
		return x;
	}

	public static <T> Page<T> clean(Page<T> x) {
		for (final T m : x.getContent()) {
			clean(m);
		}
		return x;
	}

}
