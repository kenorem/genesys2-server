/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.util.Calendar;

import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import org.genesys2.server.model.impl.ActivityPost;
import org.genesys2.server.service.ContentService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/content/activitypost")
public class ActivityPostController extends BaseController {

	@Autowired
	private ContentService contentService;

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping("/new")
	public String newPost(ModelMap model) {
		model.addAttribute("activityPost", new ActivityPost());

		return "/content/activitypost-edit";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping("{id}/edit")
	public String update(ModelMap model, @PathVariable("id") long id) {
		final ActivityPost activityPost = contentService.getActivityPost(id);
		if (activityPost == null) {
			throw new ResourceNotFoundException();
		}

		model.addAttribute("activityPost", activityPost);

		return "/content/activitypost-edit";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping("{id}/delete")
	public String delete(ModelMap model, @PathVariable("id") long id) {
		contentService.deleteActivityPost(id);

		return "redirect:/";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping(value = "/update", params = { "id" }, method = { RequestMethod.POST })
	public String updatePost(ModelMap model, @RequestParam("id") long id, @RequestParam("title") String title, @RequestParam("body") String body) {
		contentService.updateActivityPost(id, title, body);

		return "redirect:/";
	}

	@RequestMapping(value = "/update", method = { RequestMethod.POST })
	public String postPost(ModelMap model, @RequestParam("title") String title, @RequestParam("body") String body) {
		contentService.createActivityPost(title, body);

		return "redirect:/";
	}

	@RequestMapping(value = "/add-post", method = { RequestMethod.PUT }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public JsonActivityPost putPost(ModelMap model, @RequestBody @Validated JsonActivityPost newPost) {
		_logger.debug("Adding post: " + newPost);

		return null;
	}

	public static class JsonActivityPost {
		public Long id;

		@NotNull(message = "sample.error.not.null")
		@NotEmpty(message = "sample.error.not.empty")
		public String title, body;

		public Calendar postDate;

		@Override
		public String toString() {
			return "ActivityPost title=" + title + " id=" + id;
		}
	}
}
