/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.DistributedObject;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.monitor.LocalMapStats;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepository;
import org.genesys2.server.service.*;
import org.genesys2.server.service.impl.ContentSanitizer;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.worker.ElasticUpdater;
import org.genesys2.server.service.worker.ITPGRFAStatusUpdater;
import org.genesys2.server.service.worker.InstituteUpdater;
import org.genesys2.server.service.worker.SGSVUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Controller
@RequestMapping("/admin")
@PreAuthorize("hasRole('ADMINISTRATOR')")
public class AdminController {
	public static final Log LOG = LogFactory.getLog(AdminController.class);
	@Autowired
	InstituteUpdater instituteUpdater;

	@Autowired
	CountryNamesUpdater alternateNamesUpdater;

	@Autowired
	ElasticUpdater elasticUpdater;

	@Autowired
	GenesysService genesysService;

	@Autowired
	GeoService geoService;

	@Autowired
	SGSVUpdate sgsvUpdater;

	@Autowired
	ContentSanitizer contentSanitizer;

	@Autowired
	ITPGRFAStatusUpdater itpgrfaUpdater;

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private GenesysLowlevelRepository genesysLowlevelRepository;

	@Autowired
	private MappingService mappingService;

	@Autowired
	private CacheManager cacheManager;

	@Autowired
	private TaskExecutor taskExecutor;

	@Autowired
	private GenesysFilterService filterService;

	@Value("${list.logger.pagination.size}")
	int size;

	private Logger logger;

	@RequestMapping("/")
	public String root(Model model) {
		return "/admin/index";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/refreshWiews")
	public String refreshWiews() {
		try {
			instituteUpdater.updateFaoInstitutes();
		} catch (final IOException e) {
			LOG.error(e);
		}
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/refreshCountries")
	public String refreshCountries() {
		try {
			geoService.updateCountryData();
		} catch (final IOException e) {
			LOG.error(e.getMessage(), e);
		}
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/reindex-elastic")
	public String reindexElastic(@RequestParam(value = "startAt", required = false) Long startAt,
			@RequestParam(value = "slow", required = true, defaultValue = "true") boolean slow) {
		// LOG.info("Json filter: " + jsonFilter);
		elasticUpdater.fullReindex();
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/reindex-elastic", params = { "filter" })
	public String reindexElasticFiltered(@RequestParam(value = "filter", required = true) String jsonFilter,
			@RequestParam(value = "slow", required = false, defaultValue = "true") boolean slow) throws IOException {

		ObjectMapper mapper = new ObjectMapper();

		AppliedFilters filters = mapper.readValue(jsonFilter, AppliedFilters.class);

		genesysLowlevelRepository.listAccessionIds(filters, null, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				long accessionId = rs.getLong(1);

				elasticUpdater.update(Accession.class, accessionId);
			}
		});

		LOG.info("Done.");

		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/clear-queues")
	public String clearElasticQueues() {
		elasticUpdater.clearQueues();
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/updateAccessionCountryRefs")
	public String updateAccessionCountryRefs() {
		genesysService.updateAccessionCountryRefs();
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/refreshMetadataMethods")
	public String refreshMetadataMethods() {
		genesysService.refreshMetadataMethods();
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/updateInstituteCountryRefs")
	public String updateInstituteCountryRefs() {
		instituteService.updateCountryRefs();
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/updateAccessionInstituteRefs")
	public String updateAccessionInstituteRefs() {
		genesysService.updateAccessionInstitueRefs();
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/updateSGSV")
	public String updateSGSV() {
		sgsvUpdater.updateSGSV();
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/adjustLogger")
	public String adjustLogger(Model model, @RequestParam(value = "page", defaultValue = "1") Integer page){
		int size = this.size;

		//first element on page
		int firstelement = (page==null)?0:(page-1) * size;

		List<Logger> allLoggers = getAllLoggers();
		int totalPage = allLoggers.size()/size + 1;

		List<Logger> loggers = new ArrayList<>();

		if(size > (allLoggers.size() - firstelement)){
			size = allLoggers.size() - firstelement;
		}

		//copy part of array
		for(int i = 0; i<size; i++){
			loggers.add(allLoggers.get(firstelement+i));
		}

		model.addAttribute("loggers", loggers);
		model.addAttribute("pageNumber", page);
		model.addAttribute("totalPage", totalPage);
		return "/admin/adjust-logger/loggersList";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/adjustLoggerPage/{loggerName}")
	public String adjustLoggerPage(Model model, @PathVariable(value = "loggerName")String loggerName){
		if (loggerName != null && "root".equalsIgnoreCase(loggerName))
			logger = LogManager.getRootLogger();
		else
			logger = LogManager.getLogger(loggerName);
		model.addAttribute("logger", logger);
		model.addAttribute("appenders", logger.getRootLogger().getAllAppenders());
		return "/admin/adjust-logger/adjustLoggerPage";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/changeLoger")
	public String changeLogger(@RequestParam(value = "loggerLevel") String loggerLevel,
							   @RequestParam(value = "loggerName") String loggerName){
		if (loggerName != null && "root".equalsIgnoreCase(loggerName))
			logger = LogManager.getRootLogger();
		else
			logger = LogManager.getLogger(loggerName);
		if (logger != null) {
			LOG.debug("Got logger: " + logger.getName());
			logger.setLevel(loggerLevel == null ? null : Level.toLevel(loggerLevel));
		}
		return "redirect:/admin/adjustLoggerPage/" + loggerName+".";
	}

	private List<Logger> getAllLoggers(){

		Enumeration<Logger> en = LogManager.getCurrentLoggers();
		List<Logger> loggers = new ArrayList<>();

		while (en.hasMoreElements()) {
			loggers.add(en.nextElement());
		}

		loggers.add(LogManager.getRootLogger());

		Collections.sort(loggers, new Comparator<Logger>() {
			@Override
			public int compare(Logger o1, Logger o2) {
//				root logger is on the top
				if (LogManager.getRootLogger() == o1){
					return -1;
				}
				if (LogManager.getRootLogger() == o2){
					return 1;
				}
//				otherwise sort by name
				return o1.getName().compareTo(o2.getName());
			}
		});

		return loggers;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/addLoger")
	public String addLogger(@RequestParam(value = "nameNewLogger") String nameNewLogger, Model model){
		Logger.getLogger(nameNewLogger);
		return "redirect:/admin/adjustLogger";
	}



	@RequestMapping(method = RequestMethod.POST, value = "/convertNames")
	public String convertNames() {
		// Convert {@link AllAccenames} to Aliases
		final List<Object[]> list = new ArrayList<Object[]>(100000);
		genesysLowlevelRepository.listAccessionsAccenames(new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				final long accessionId = rs.getLong(1);
				final String acceNames = rs.getString(2);
				final String otherIds = rs.getString(3);
				list.add(new Object[] { accessionId, acceNames, otherIds });
				if (list.size() % 10000 == 0) {
					LOG.info("Loaded names: " + list.size());
				}
			}
		});

		int i = 0;
		for (final Object[] o : list) {
			if (++i % 1000 == 0) {
				LOG.info("Conversion progress " + i + " of " + list.size());
			}
			genesysService.upsertAliases((long) o[0], (String) o[1], (String) o[2]);
		}

		list.clear();

		System.err.println("FOOBAR!");

		final Set<Long> toRemove = new HashSet<Long>();

		// Remove stupid stuff
		// List<Long> aliasesToRemove = new ArrayList<Long>();
		genesysLowlevelRepository.listAccessionsAlias(new RowCallbackHandler() {
			private long prevAccnId = -1;
			private final List<Object[]> aliases = new ArrayList<Object[]>(10);

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				System.err.println("..");
				// n.accessionId, n.instCode, n.name, n.aliasType, n.lang,
				// n.version
				if (prevAccnId == rs.getLong(1) || prevAccnId == -1) {
					prevAccnId = rs.getLong(1);
					System.err.println("Add... " + prevAccnId + " " + rs.getLong(1));
				} else {
					cleanup(prevAccnId, aliases);
					aliases.clear();
					prevAccnId = rs.getLong(1);
				}
				aliases.add(new Object[]{rs.getLong(7), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5)});
			}

			private void cleanup(long accessionId, List<Object[]> existingAliases) {
				System.err.println("CLEANUP:");
				for (final Object[] alias : existingAliases) {
					System.err.println("" + accessionId + " = " + ArrayUtils.toString(alias, "NULL"));
				}

				for (int i = 0; i < existingAliases.size() - 1; i++) {
					final Object[] name1 = existingAliases.get(i);
					if (toRemove.contains(name1[0])) {
						continue;
					}
					System.err.println("Base " + i + " " + ArrayUtils.toString(name1));
					for (int j = i + 1; j < existingAliases.size(); j++) {
						System.err.println("Inspecting " + j);
						final Object[] name2 = existingAliases.get(j);
						if (toRemove.contains(name2[0])) {
							continue;
						}
						final int res = whatToKeep(name1, name2);
						if (res == -1) {
							System.err.println("Would remove " + i + " " + ArrayUtils.toString(name1));
							toRemove.add((long) name1[0]);
						} else if (res == 1) {
							System.err.println("Would remove " + j + " " + ArrayUtils.toString(name2));
							toRemove.add((long) name2[0]);
						}
					}
				}
			}

			private int whatToKeep(Object[] name1, Object[] name2) {
				if (StringUtils.equals((String) name1[2], (String) name2[2])) {
					final float score1 = score(name1), score2 = score(name2);
					if (score1 < score2) {
						return -1;
					} else {
						return 1;
					}
				} else {
					return 0;
				}
			}

			private float score(Object[] name1) {
				float score = 1.0f;
				if (name1[1] != null) {
					score += 2;
					if ((int) name1[3] == 5) {
						score *= 2;
					}
				} else {
					if ((int) name1[3] == 0) {
						score += 1;
					}
				}
				return score;
			}
		});

		this.genesysService.removeAliases(toRemove);

		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/sanitize")
	public String sanitize() {
		LOG.info("Sanitizing content");
		contentSanitizer.sanitizeAll();
		LOG.info("Sanitizing content.. Done");
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/updateAlternateNames")
	public String updateAlternateNames() {
		LOG.info("Updating alternate GEO names");
		try {
			alternateNamesUpdater.updateAlternateNames();
		} catch (final IOException e) {
			LOG.error(e.getMessage(), e);
		}
		LOG.info("Updating alternate GEO names: done");
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/updateITPGRFA")
	public String updateITPGRFA() {
		LOG.info("Updating country ITPGRFA status");
		try {
			itpgrfaUpdater.downloadAndUpdate();
		} catch (final IOException e) {
			LOG.error(e.getMessage(), e);
		}
		LOG.info("Updating done");
		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/clearTilesCache")
	public String clearTilesCache() {
		final Cache tileServerCache = cacheManager.getCache("tileserver");
		System.err.println("tileServerCache=" + tileServerCache.getNativeCache());

		@SuppressWarnings("rawtypes")
		final IMap hazelCache = (IMap) tileServerCache.getNativeCache();

		LOG.info("Tiles cache size=" + hazelCache.size());
		int count = 0;
		for (final Object key : hazelCache.keySet()) {
			LOG.info("\tkey=" + key);
			if (++count > 20) {
				break;
			}
		}
		mappingService.clearCache();
		LOG.info("Tiles cache size=" + hazelCache.size());

		return "redirect:/admin/";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/clearCaches")
	public String clearCaches() {
		for (String cacheName : cacheManager.getCacheNames()) {
			final Cache cache = cacheManager.getCache(cacheName);
			LOG.info("Clearing cache " + cacheName);
			cache.clear();
		}
		return "redirect:/admin/cache";
	}
	

	@RequestMapping("/cache")
	public String cacheStats(Model model) {
		List<CacheStats> cacheMaps = new ArrayList<CacheStats>();
		List<Object> cacheOther = new ArrayList<Object>();

		Set<HazelcastInstance> instances = Hazelcast.getAllHazelcastInstances();
		for (HazelcastInstance hz : instances) {
			if (LOG.isDebugEnabled())
				LOG.debug("\n\nCache stats Instance: " + hz.getName());
			for (DistributedObject o : hz.getDistributedObjects()) {
				if (o instanceof IMap) {
					IMap imap = (IMap) o;
					cacheMaps.add(new CacheStats(imap));
					// System.out.println(imap.getServiceName() + ": " +
					// imap.getName() + " " + imap.getPartitionKey());
					// LocalMapStats localMapStats = imap.getLocalMapStats();
					// System.out.println("created: " +
					// localMapStats.getCreationTime());
					// System.out.println("owned entries: " +
					// localMapStats.getOwnedEntryCount());
					// System.out.println("backup entries: " +
					// localMapStats.getBackupEntryCount());
					// System.out.println("locked entries: " +
					// localMapStats.getLockedEntryCount());
					// System.out.println("dirty entries: " +
					// localMapStats.getDirtyEntryCount());
					// System.out.println("hits: " + localMapStats.getHits());
					// System.out.println("puts: " +
					// localMapStats.getPutOperationCount());
					// System.out.println("last update: " +
					// localMapStats.getLastUpdateTime());
					// System.out.println("last access:" +
					// localMapStats.getLastAccessTime());
				} else {
					if (LOG.isDebugEnabled())
						LOG.debug(o.getClass() + " " + o);
					cacheOther.add(o);
				}
			}
		}

		model.addAttribute("cacheMaps", cacheMaps);
		model.addAttribute("cacheOther", cacheOther);

		return "/admin/cache";
	}

	public static final class CacheStats {

		private String serviceName;
		private String name;
		private LocalMapStats mapStats;

		public CacheStats(IMap<?, ?> imap) {
			this.serviceName = imap.getServiceName();
			this.name = imap.getName();
			this.mapStats = imap.getLocalMapStats();
		}

		public String getServiceName() {
			return serviceName;
		}

		public String getName() {
			return name;
		}

		public LocalMapStats getMapStats() {
			return mapStats;
		}
	}


}
