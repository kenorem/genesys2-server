/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.kpi;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class BooleanDimension extends Dimension<Boolean> {

	@Column
	private int mode = 3;

	@JsonIgnore
	@Override
	public Set<Boolean> getValues() {
		Set<Boolean> b = new HashSet<Boolean>();
		if (hasTrue()) {
			b.add(Boolean.TRUE);
		}
		if ((mode ^ 2) > 0) {
			b.add(Boolean.FALSE);
		}
		return b;
	}

	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public boolean hasTrue() {
		return (mode & 1) > 0;
	}

	public void useTrue(boolean use) {
		if (use)
			mode |= 1;
		else
			mode &= ~1;
	}

	public boolean hasFalse() {
		return (mode & 2) > 0;
	}

	public void useFalse(boolean use) {
		if (use)
			mode |= 2;
		else
			mode &= ~2;
	}

}
