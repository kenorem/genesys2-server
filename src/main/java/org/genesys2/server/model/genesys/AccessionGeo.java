/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.BusinessModel;
import org.genesys2.server.model.impl.GeoReferencedEntity;

@Entity
@Table(name = "accessiongeo")
public class AccessionGeo extends BusinessModel implements GeoReferencedEntity, AccessionRelated {

	/**
	 *
	 */
	private static final long serialVersionUID = 8046638388176612388L;

	@Version
	private long version = 0;

	@OneToOne(optional = false, fetch = FetchType.LAZY, cascade = {})
	@JoinColumn(name = "accessionId", unique = true, nullable = false, updatable = false)
	private Accession accession;

	@Column(name = "longitude")
	private Double longitude;
	@Column(name = "latitude")
	private Double latitude;
	@Column(name = "elevation")
	private Double elevation;

	private Double uncertainty;
	private String datum;
	private String method;

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public Accession getAccession() {
		return accession;
	}

	public void setAccession(Accession accession) {
		this.accession = accession;
	}

	@Override
	public Double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(final Double longitudeD) {
		this.longitude = longitudeD;
	}

	@Override
	public Double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(final Double latitudeD) {
		this.latitude = latitudeD;
	}

	@Override
	public Double getElevation() {
		return elevation;
	}

	public void setElevation(Double elevation) {
		this.elevation = elevation;
	}

	public void setUncertainty(Double uncertainty) {
		this.uncertainty = uncertainty;
	}

	public Double getUncertainty() {
		return uncertainty;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public String getDatum() {
		return datum;
	}

	/**
	 * Set geodetic method
	 * 
	 * @param method
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	public String getMethod() {
		return method;
	}

	public boolean isEmpty() {
		if (StringUtils.isNotBlank(datum))
			return false;
		if (StringUtils.isNotBlank(method))
			return false;
		if (this.latitude != null || this.longitude != null || this.elevation != null || this.uncertainty != null)
			return false;
		return true;
	}
}
