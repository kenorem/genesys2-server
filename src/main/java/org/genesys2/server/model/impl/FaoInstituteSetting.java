/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.genesys2.server.model.VersionedAuditedModel;
import org.hibernate.annotations.Type;

/**
 * Extra configuration for {@link FaoInstitute}
 */
@Entity
@Table(name = "faoinstitutesetting", uniqueConstraints = @UniqueConstraint(columnNames = { "instCode", "setting" }))
public class FaoInstituteSetting extends VersionedAuditedModel {
	/**
	 *
	 */
	private static final long serialVersionUID = 4782380146065111989L;
	@Column(length = 7, nullable = false)
	private String instCode;
	@Column(length = 64)
	private String setting;
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String value;

	public FaoInstituteSetting() {
	}

	public FaoInstituteSetting(FaoInstitute faoInstitute) {
		this.instCode = faoInstitute.getCode();
	}

	public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}

	public String getSetting() {
		return setting;
	}

	public void setSetting(String setting) {
		this.setting = setting;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
