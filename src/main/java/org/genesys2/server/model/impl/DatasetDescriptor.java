/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.genesys2.server.model.BusinessModel;

@Entity
@Table(name = "datasetdescriptor", uniqueConstraints = { @UniqueConstraint(columnNames = { "datasetId", "descriptorId" }) })
public class DatasetDescriptor extends BusinessModel {

	private static final long serialVersionUID = 2413430585742976014L;

	@ManyToOne(optional = false)
	@JoinColumn(name = "datasetId")
	private Dataset dataset;

	@Column(nullable = false)
	private int orderIndex;

	@ManyToOne(optional = false)
	@JoinColumn(name = "descriptorId")
	private Descriptor descriptor;

	public Dataset getDataset() {
		return dataset;
	}

	public void setDataset(Dataset dataset) {
		this.dataset = dataset;
	}

	public int getOrderIndex() {
		return orderIndex;
	}

	public void setOrderIndex(int orderIndex) {
		this.orderIndex = orderIndex;
	}

	public Descriptor getDescriptor() {
		return descriptor;
	}

	public void setDescriptor(Descriptor descriptor) {
		this.descriptor = descriptor;
	}
}
