/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import java.util.List;

import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Team;
import org.genesys2.server.model.impl.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

public interface TeamService {

	/**
	 * Add team to the system
	 *
	 * @param name
	 *            a unique {@link Team} name
	 * @return a Team
	 */
	Team addTeam(String name);

	/**
	 * Add a User to the team
	 *
	 * @param teamUuid
	 * @param user
	 * @return
	 */
	@PreAuthorize("hasRole('ADMINISTRATOR') || hasPermission(#team, 'CREATE')")
	Team addTeamMember(String teamUuid, User user);

	/**
	 * Remove User from the Team
	 *
	 * @param teamUuid
	 * @param user
	 */
	@PreAuthorize("hasRole('ADMINISTRATOR') || hasPermission(#team, 'CREATE')")
	Team removeTeamMember(String teamUuid, User user);

	/**
	 * Remove current user from team
	 *
	 * @param team
	 */
	void removeMe(Team team);

	void removeMe(long teamId);

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	Team addTeamInstitute(Team team, FaoInstitute institute);

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	Team removeTeamInsitute(Team team, FaoInstitute institute);

	/**
	 * List user's teams
	 *
	 * @return
	 */
	List<Team> listMyTeams();

	/**
	 * List user's teams
	 *
	 * @param user
	 * @return
	 */
	List<Team> listUserTeams(User user);

	/**
	 * List all teams
	 *
	 * @return
	 */
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	Page<Team> listTeams(Pageable pageable);

	@PostAuthorize("hasRole('ADMINISTRATOR') or hasPermission(returnObject, 'READ')")
	Team getTeam(String uuid);

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#team, 'READ')")
	List<User> getMembers(Team team);

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasPermission(#team, 'WRITE')")
	void updateTeamInformation(String teamUuid, String teamName);
}
