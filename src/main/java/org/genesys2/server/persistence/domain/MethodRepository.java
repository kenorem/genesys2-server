/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.List;

import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.genesys.Parameter;
import org.genesys2.server.model.impl.Crop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MethodRepository extends JpaRepository<Method, Long> {

	List<Method> findByFieldType(int fieldType);

	List<Method> findByParameter(Parameter parameter);

	Method findByRdfUri(String rdfUri);

	Method findByMethodAndParameter(String method, Parameter parameter);

	Method findByIdAndParameter(long id, Parameter findOne);

	@Query("select distinct m from Method m where m.id in ( ?1 )")
	List<Method> findByIds(List<Long> oids);

	@Query("select m from Method m where m.parameter.crop = ?1")
	List<Method> findByCrop(Crop crop);
}
