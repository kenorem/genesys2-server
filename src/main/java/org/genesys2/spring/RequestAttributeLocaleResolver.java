/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/*
 * Copyright 2002-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.spring;

import org.genesys2.server.servlet.filter.LocaleURLFilter;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;
import java.util.Set;

/**
 * Implementation of LocaleResolver that uses a locale attribute in the user's
 * session in case of a custom setting, with a fallback to the specified default
 * locale or the request's accept-header locale.
 * 
 * <p>
 * This is most appropriate if the application needs user sessions anyway, that
 * is, when the HttpSession does not have to be created for the locale.
 * 
 * <p>
 * Custom controllers can override the user's locale by calling
 * {@code setLocale}, e.g. responding to a locale change request.
 * 
 * @author Juergen Hoeller
 * @author Matija Obreza
 * 
 * @see #setDefaultLocale
 * @see #setLocale
 */
public class RequestAttributeLocaleResolver implements LocaleResolver {

	private Locale defaultLocale;
    private Set<String> supportedLocales;

	@Override
	public Locale resolveLocale(HttpServletRequest request) {
		Locale requestAttributeLocale = (Locale) request.getAttribute(LocaleURLFilter.REQUEST_LOCALE_ATTR);

		if (requestAttributeLocale == null) {
			return this.defaultLocale;
		}

		return requestAttributeLocale;
	}

	@Override
	public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
		throw new UnsupportedOperationException("Cannot set locale!");
	}

    public Set<String> getSupportedLocales() {
        return supportedLocales;
    }

    public void setSupportedLocales(Set<String> supportedLocales) {
        this.supportedLocales = supportedLocales;
    }

    public void setDefaultLocale(Locale defaultLocale) {
		this.defaultLocale=defaultLocale;
	}
}
