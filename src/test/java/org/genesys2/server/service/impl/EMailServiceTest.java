package org.genesys2.server.service.impl;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class EMailServiceTest {
	EMailServiceImpl emailService = new EMailServiceImpl();

	@Test
	public void testToEmailsSplit() {
		String[] res = null;
		res = emailService.toEmails("");
		assertTrue(res == null);
		res = emailService.toEmails("test1");
		assertTrue(res != null);
		assertTrue(res.length == 1);
		assertTrue("test1".equals(res[0]));

		res = emailService.toEmails("test1,test2");
		assertTrue(res != null);
		assertTrue(res.length == 2);
		assertTrue("test1".equals(res[0]));
		assertTrue("test2".equals(res[1]));

		res = emailService.toEmails("test1;test2");
		assertTrue(res != null);
		assertTrue(res.length == 2);
		assertTrue("test1".equals(res[0]));
		assertTrue("test2".equals(res[1]));
		
		res = emailService.toEmails("test1  ; test2,test3   ");
		assertTrue(res != null);
		assertTrue(res.length == 3);
		assertTrue("test1".equals(res[0]));
		assertTrue("test2".equals(res[1]));
		assertTrue("test3".equals(res[2]));
	}
}
