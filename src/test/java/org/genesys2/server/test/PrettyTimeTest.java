package org.genesys2.server.test;

import java.util.Date;
import java.util.Locale;

import org.junit.Test;
import org.ocpsoft.prettytime.PrettyTime;

public class PrettyTimeTest {
	@Test
	public void testPretty() {
		PrettyTime pt=new PrettyTime();
		Date then = new Date();
		System.out.println(pt.format(then));
	}
	
	@Test
	public void testPrettyLocaleDe() {
		Locale locale=Locale.GERMAN;
		PrettyTime pt=new PrettyTime(locale);
		Date then = new Date();
		System.out.println(pt.format(then));
	}
}
