
# Genesys PGR - Gateway to genetic resources

**genesys2-server** configuration options can be found in 
`src/main/resources/spring/spring.properties` file.

You will need to configure at least the database connection url and credentials before starting up Genesys server.

## A very quick start

* Create a blank mysql database
* Configure genesys2-server database connection settings
* Start Jetty with `mvn jetty:run`

### Running Genesys on local mysql instance

Connect to your mysql instance and create a new blank database **genesys2**:

```mysql
# Create database
CREATE DATABASE genesys2 DEFAULT CHARSET UTF8;
# Create user genesys with password pwd
GRANT ALL ON genesys2.* TO 'genesys'@'localhost' IDENTIFIED BY 'pwd';
```

Change the settings in `src/main/resources/spring/spring.properties`:

```properties
# Database genesys2 on localhost
db.url=jdbc:mysql://localhost/genesys2?useUnicode=true&characterEncoding=UTF-8&useFastDateParsing=false
# mysql driver
db.driverClassName=com.mysql.jdbc.Driver
# InnoDB dialect
db.dialect=org.hibernate.dialect.MySQL5Dialect
# Username and password
db.username=genesys
db.password=pwd
# Log SQL commands to console
db.showSql=false
# Allow hibernate to update database (development/testing mode only)
db.hbm2ddl=update
```


### Start Genesys with Maven

Jetty servlet container is used to run Genesys from Maven.

```sh
    $ mvn jetty:run
```

